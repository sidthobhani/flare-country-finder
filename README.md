## LAUNCHING THE APPLICATION
## Development server
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
## Code scaffolding
Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.
## Build
Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.
## Running unit tests
Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).
## Running end-to-end tests
Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
## Further help
To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

## APPLICATION FILES SYNOPSIS
## app/app.component.ts
Loads country-finder.component.html in to the DOM and handles the DB API calls & data binding with the view
## app/app.module.ts
Imports the required libraries and modules
## app/country-finder.component.html
HTML for the app
## app/data.service.ts
Imports HTTP client module
## assets/images/flarelogo.png
Logo image used in the header
## model/countryfinder.model.ts
Countries model class declaration containing the fields flag, name, currencies, latlag, alpha3Code and area
## style/country-finder.scss
Styling for the app
## style/country-finder-vars.scss
SCSS vars for the country-finder.scss
## favicon.ico
Favicon for the web page
## index.html
Application entry page