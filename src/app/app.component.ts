import { Component } from '@angular/core';
import { Countries } from "../model/countryfinder.model";
import {DataService} from "./data.service";

@Component({
  selector: 'app-root',
  templateUrl: './country-finder.component.html',
  styles: []
})
export class AppComponent{
  countries$: Countries[] = [];
  countriesHistory$: Countries[] = [];
  selectedCountry$: Countries;
  historyChecked: boolean = false;
  constructor(private dataService: DataService){};
  ngOnInit() {};

  //On Country text input
  onCountryFinderInputChange(evt){
      //User input
      let userInput:string = evt.currentTarget.value;
      this.selectedCountry$ = this.countries$.find(o => o.name === userInput.substr(0, userInput.indexOf("-")).trim());
      //Store country ojbect in history if not already stored, new item appears first (at the top)
      if(this.selectedCountry$ !== undefined){
        let countryNameInHistoryCollection = this.countriesHistory$.find(o => o.name === userInput.substr(0, userInput.indexOf("-")).trim());
        if(countryNameInHistoryCollection === undefined) {
          this.countriesHistory$.unshift(this.selectedCountry$);
        }
      }
      this.countries$ = [];
      //Get country from the DB API
      if(userInput.length >= 3 && this.selectedCountry$ === undefined) {
        return this.dataService.getCountries(userInput)
            .subscribe(data => this.countries$ = data.splice(0,10));
      }
  };

  //On history toggle
  onHistoryCBChange(evt){
    this.selectedCountry$ = undefined;
    this.historyChecked = evt.currentTarget.checked;
  };

  //On country name clicked in history list
  onCountryListItemClick(evt){
    this.selectedCountry$ = this.countriesHistory$.find(o => o.name === evt.currentTarget.innerText);
  }
}
