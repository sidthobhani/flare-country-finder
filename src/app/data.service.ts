import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import { Countries } from "../model/countryfinder.model";

@Injectable({
  providedIn: 'root'
})
export class DataService {
  apiUrl = "https://restcountries.eu/rest/v2/name/";
  constructor(private _http: HttpClient) {}

  getCountries(userInput){
    return this._http.get<Countries[]>(this.apiUrl+""+userInput);
  }
}
