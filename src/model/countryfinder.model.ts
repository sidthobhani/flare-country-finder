export class Countries{
    flag: string;
    name: string;
    currencies: Array<any>;
    latlng: Array<any>;
    alpha3Code: string;
    area: number;
}